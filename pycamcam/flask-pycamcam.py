#!/usr/bin/env python
# coding: utf-8

# In[1]:


import camcam


# In[2]:


#camcam.takephoto('/home/pi/test')


# In[3]:


from flask import Flask, request, jsonify, render_template
#import ic

app = Flask(__name__)

@app.route('/createdb')
def createdb():
    nameofdb = request.args.get('nameofdb')
    #lenth = request.args.get('length')
    #ic.createdbtext(nameofdb)
    #ic.createdatabase(nameofdb)

    return('{} database created!'.format(nameofdb))

@app.route('/createtext', methods=['GET', 'POST'])
def createtext():
    if request.method == 'POST':
        friendname = request.form.get('friendname')
        username = request.form.get('username')
        message = request.form.get('message')
#        ic.createtext(friendname, username, message)
        return({'the friend {} left the following message for {}: {}'.format(friendname, username, message)})
    return(render_template('index.html'))

@app.route('/takephoto', methods=['GET', 'POST'])
def takephoto():
    if request.method == 'POST':

        pathdir = request.json['pathdir']
        #entime = request.args.get('lentime')

        camcam.takephoto(pathdir)
        return(pathdir)
    return('do a post request')

@app.route('/takevideo', methods=['GET', 'POST'])
def takevideo():
    if request.method == 'POST':

        sizex = request.json['sizex']
        sizey = request.json['sizey']
        direc = request.json['direc']
        recordtime = request.json['recordtime']
        #entime = request.args.get('lentime')

        camcam.createsnap(sizex, sizey, direc, recordtime)
        return('video created')
    return('do a post request')

#stopmotimg(appath, amphoto, timsleep)

@app.route('/takestopmot', methods=['GET', 'POST'])
def takestopmot():
    if request.method == 'POST':

        appath = request.json['appath']
        amphoto = request.json['amphoto']
        timsleep = request.json['timsleep']
        #recordtime = request.json['recordtime']
        #entime = request.args.get('lentime')

        camcam.stopmotimg(appath, amphoto, timsleep)

        return('stop motion created')
    return('do a post request')



@app.route('/dirtogif', methods=['GET', 'POST'])
def dirtogif():
    if request.method == 'POST':

        whatdir = request.json['whatdir']
        toptext = request.json['toptext']
        bottext = request.json['bottext']
        savedir = request.json['savedir']
        savfname = request.json['savfname']
        #recordtime = request.json['recordtime']
        #entime = request.args.get('lentime')

        camcam.extdirtogifs(whatdir, toptext, bottext, savedir, savfname)

        return('dir saved with top text and bottom text in dir specified')
    return('do a post request')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5556)


# In[ ]:




